//Ekspresi Class
var Car = class {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
  }
};
console.log(Car.name);

var Car = class Car2 {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
  }
};
console.log(Car.name);
//Method
class Car1 {
  constructor(brand) {
    this.carname = brand;
  }
  present() {
    return "I have a " + this.carname;
  }
}
mycar = new Car1("Ford");
console.log(mycar.present());
