var personArr = ["John", "Doe", "Male", 27]
var personObj = {
    firstName: "John",
    lastName: "Doe",
    gender: "Male",
    age: 27
}
console.log(personObj.firstName);
console.log(personArr[0]);

//Deklarasi Object

var car = {
    brand: "Ferrari",
    type: "Sport Car",
    price: 500000000,
    'horse power': 986
}
var car2 = {}
//meng-assign key:value dari object car2
car2.brand = "Lamborghini"
car2.brand = "Sport Car"
car2.price = 100000000

car2["horse power"] = 730

console.log(car2);
console.log(car);

//Mengakses Nilai pada Object
var motorcycle1 = {
    brand: "Handa",
    type: "CB",
    price: 1000
}
console.log(motorcycle1.brand);
console.log(motorcycle1.type);
console.log(motorcycle1["price"]);