//Contoh Looping WHile-loop 1
var flag = 1;
while (flag < 10) {
  console.log("Iterasi ke- " + flag);
  flag++;
}
//Contoh Looping While-Loop 2
var deret = 5;
var jumlah = 0;
while (deret > 0) {
  jumlah += deret;
  deret--;
  console.log("Jumlah Saat Ini: " + jumlah);
}
console.log(jumlah);
//For-Loop 1
for (var angka = 1; angka < 10; angka++) {
  console.log("Iterasi ke-" + angka);
}
//For-Loop 2
var jumlahe = 0;
for (var derete = 5; derete > 0; derete--) {
  jumlahe += derete;
  console.log("Jumlah Saat Ini : " + jumlahe);
}
console.log("Jumlah Terakhir: " + jumlahe);
//For-Loop 3
for (var dere = 0; dere < 10; dere += 2) {
  console.log("Iterasi dengan Increment counter 2: " + dere);
}
console.log("..........................................");
for (var dere = 15; dere > 0; dere -= 3) {
  console.log("Iterasi dengan Decrement counter : " + dere);
}
