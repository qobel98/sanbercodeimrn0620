//Array adalah kumpulan atau tumpukan berbagai data. Cara menuliskan array yaitu dengan kurung siku ([]) 
//dan elemen-elemen nya dipisah menggunakan tanda koma (,). Setiap elemen dari array memiliki indeks yang 
//dimulai dari 0, 1, 2, dst. Kita dapat memanipulasi array dengan berbagai cara seperti menambahkan dan mengeluarkan 
//elemen dalam array, menggabungkan array, atau bahkan menghapus seluruh elemen Array nya. Kita bisa memasukkan beberapa tipe data
// yang berbeda ke dalam Array bahkan memasukkan Array ke dalam Array.


//Array juga memiliki property .length seperti pada string yang berarti panjang dari sebuah array
// var hobbies = ["coding", "cycling", "climbing", "skateboard"]
// console.log(hobbies)
// console.log(hobbies.length)

// console.log(hobbies[0])
// console.log(hobbies[2])
// console.log(hobbies.length - 1)

// //METODE ARRAY
// //1.push: menambah 1 nilai ke array ke index paling belakang
// //2.pop: menghapus 1 nilai dari array index paling belakang
// //3.unshift: menambah 1 nilai ke array index paling depan (index 0)
// //4.shift: menghapus 1 nilai dari array index paling depan (index 0)
// //5.join: menggabungkan seluruh element array menjadi sebuah string dan mengambil parameter sebagai simbol penyambung antar elemen
// //6.sort : mengurutkan elemen di dalam array sesuai aphabet
// //6.slice : menngambil beberapa lapis data
// //7.splice : mengubah nilai array dengan menghapus dan/atau menambah nilai baru ke array
// //8.split : memecah string dan mengembalikan array sesuai dengan separator/pemisah yang didefinisikan

// //cara menggunakan metode array adalah dengan menggunakan tanda dot(.)contohnya:
// var feeling = ["dag", "dig"]
// feeling.push("dug", "deg")
// feeling.pop()
// console.log(feeling)

// //.Push()
// //push adalah metode array untuk melakukan nilai dibelakang elemen terakhir di array. metode push menerima sebuah parameter yaitu nilai yang ingin kita tambahkan ke dalam array.
// var numbers = [0, 1, 2]
// numbers.push(3)
// console.log(numbers)
// numbers.push(4, 5)
// console.log(numbers)

// //.pop()
// var numbers = [0, 1, 2, 3, 4, 5]
// numbers.pop()
// console.log(numbers)

// //.unshift()
// var number = [0, 1, 2, 3]
// number.unshift(-1)
// console.log(number)

// //.shift
// var number1 = [0, 1, 2, 3]
// number1.shift()
// console.log(number1)

// //.sort
// var animals = ["kera", "gajah", "musang"]
// var huruf = ["k", "b", "y", "w"]
// animals.sort()
// huruf.sort()
// console.log(animals)
// console.log(huruf)
//.sort angka
var num = [1, 21, 3, 12, 13]
num.sort(function (a, b) {
    return a - b
}) //ascending
console.log(num)
num.sort(function (a, b) {
    return b - a
}) //descending
console.log(num)

//.slice
var pizza = ["peperone", "chese", "bacon", "mozarila", "fillet"]
console.log(pizza,
    "Sebelum dislice")
var chese = pizza.slice(2, 3)
console.log(chese)
console.log(pizza,
    "Sebelum dislice")
var fillet = pizza.slice(1, 2)
console.log(fillet)
console.log(pizza,
    "Sebelum dislice")

//.splice
var fruits = ["banana", "anggur", "jeruk"]
fruits.splice(1, 0, "semangka")
console.log(fruits, "=>sudah ditambahkan semangka")
console.log(fruits.splice(1, 1))
console.log(fruits, "=>setelah dibuang watemelon")


//.split dan join
//=> Split = Mengubah string menjadi array
//Split menerima sebuah parameter berupa karakter yang menjadi separator untuk memecah string. 
var biodata = "name:john,hoe"
var name = biodata.split(":")
console.log(name)
//Metode join yaitu kebalikan dari split yaitu mengubah sebuah array menjadi string 
var title = ["My", "First", "Experience", "as", "programer"]
var slug = title.join("-")
console.log(slug)