//Multidimensional array atau array multidimensi adalah array yang berisi array di dalamnya. 
//Jumlah dimensi bergantung seberapa dalam array tersebut memiliki array di dalamnya.
// Cara akses nilai dari array multidimensi sama seperti array satu dimensi seperti biasa namun jumlah indeksnya terdapat sebanyak dimensi nya.

var arrayMulti = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]
// Maka sebagai gambaran, indeks dari array tersebut adalah 
/*
    [
        [(0,0), (0,1), (0,2)],
        [(1,0), (1,1), (1,2)],
        [(2,0), (2,1), (2,2)]
    ] 
*/
console.log(arrayMulti[0, 0]) // 1 
console.log(arrayMulti[1, 0]) // 4
console.log(arrayMulti[2, 1]) // 8

//Membuat array menggunakan looping
var tampung = []
for (var i = 0; i < 10; i++) {
    tampung.push(i + 1)
    console.log(tampung, "Ini Line ke- l", i)
}
console.log(tampung)