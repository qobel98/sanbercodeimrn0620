//Soal No.1(Membuat Kalimat)
var word = 'JavaScript '
var second = 'is '
var third = 'Awesome '
var fourth = 'and '
var fifth = 'I '
var sixth = 'Love '
var seventh = 'it!'

console.log(word.concat(second).concat(third).concat(fourth).concat(fifth).concat(sixth).concat(seventh))

//Soal No.2 Mengurai Kalimat(Akses Karakter dalam String)
var sentence = "I am going to be react Native Developer"

var exampleFirstWord = sentence[0]
var exampleSecondWord = sentence[2] + sentence[3]
var thirdword = sentence.substr(5, 5)
var fourthword = sentence.substr(10, 3)
var fifthword = sentence.substr(13, 3)
var sixthword = sentence.substr(16, 6)
var seventhword = sentence.substr(22, 7)
var eightword = sentence.substr(29, 10)

console.log('First Word: ' + exampleFirstWord)
console.log('Second Word: ' + exampleSecondWord)
console.log('Third Word: ' + thirdword)
console.log('Fourth Word: ' + fourthword)
console.log('Fifth Word: ' + fifthword)
console.log('Sixth Word: ' + sixthword)
console.log('Seventh Word: ' + seventhword)
console.log('Eight Word: ' + eightword)

//Soal No.3 Mengurai Kalimat(Substring)
var sentence2 = 'wow JavaScript is so cool';

var exampleFirstWord2 = sentence2.substring(0, 3);
var secondWord2 = sentence2.substr(4, 11);
var thirdWord2 = sentence2.substr(14, 3);
var fourthWord2 = sentence2.substr(18, 3);
var fifthWord2 = sentence2.substr(21, 5);

console.log('First Word: ' + exampleFirstWord2);
console.log('Second Word: ' + secondWord2);
console.log('Third Word: ' + thirdWord2);
console.log('Fourth Word: ' + fourthWord2);
console.log('Fifth Word: ' + fifthWord2);

//Soal No.4 Mnegurai Kalimat dan Menentukan Panjang String
var sentence3 = 'wow JavaScript is so cool';

var exampleFirstWord3 = sentence3.substring(0, 3);
var secondWord3 = sentence3.substr(4, 11);
var thirdWord3 = sentence3.substr(14, 3);
var fourthWord3 = sentence3.substr(18, 3);
var fifthWord3 = sentence3.substr(21, 5);

var firstWordLength = exampleFirstWord3.length
var secondWordLength = secondWord3.length
var thirdWordLength = thirdWord3.length
var fourdWordLength = fourthWord3.length
var fifthWordLength = fifthWord3.length

console.log('First Word: ' + exampleFirstWord3 + ', With Length: ' + firstWordLength);
console.log('Second Word: ' + secondWord3 + ', With Length: ' + secondWordLength);
console.log('Third Word: ' + thirdWord3 + ', With Length: ' + thirdWordLength);
console.log('Fourth Word: ' + fourthWord3 + ', With Length: ' + fourdWordLength);
console.log('Fifth Word: ' + fifthWord3 + ', With Length: ' + fifthWordLength);