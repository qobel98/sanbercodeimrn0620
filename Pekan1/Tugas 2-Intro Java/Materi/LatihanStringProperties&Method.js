//Pengenalan String
//String adalah tipe data yang berisi karakter-karakter dibungkus dalam tanda petik ("" atau '' ).
//Karakter-karakter pada suatu string dapat diakses dengan menggunakan indeks atau posisi karakter berada.
//Indeks pada string selalu mulai dari 0.
var sentences = "JavaSricpt";
console.log(sentences[1]);
console.log(sentences[4]);

//String Properties
//.Length
var word = "Javasript is Awesome";
console.log(word.length);

//String Methods
//.CharAt([Indeks])
console.log("I Am a String".charAt(5));

//.concat([string])
var string1 = "good";
var string2 = "luck";
console.log(string1.concat(string2));

//.indexOf([string/karakter])
var text = "dung dung ces!";
console.log(text.indexOf("dung"));
console.log(text.indexOf("u"));
console.log(text.indexOf("Jreng"));

//.substring([indeks awal], [indeks akhir (optional)])
var car1 = "Lykan Hypershot";
var car2 = car1.substr(6);
console.log(car2);

//.substr([indeks awal], [jumlah karakter yang diambil (optional)])
var motor1 = "Gegana Motor";
var motor2 = motor1.substr(2, 2);
console.log(motor2);

//.toUpperCase()
var letter = "This Letter present to You";
var upper = letter.toUpperCase();
console.log(upper);

//.toLowerCase()
var letter = "This Letter Present TO You";
var lower = letter.toLowerCase();
console.log(lower);

//.trim()
var username = "Admin user";
var newusername = username.trim();
console.log(newusername);

//Mengubah Tipe data dari dan ke String

//String([angka/array])
//Fungsi Global String() dapat dipanggil kepan saja pada program JavaScipt dan akan mengembalikan data dalam tipe data string dari parameter yang diberikan
var int = 12;
var real = 3.45;
var arr = [6, 7, 8];

var strInt = String(int);
var strReal = String(real);
var strArr = String(arr);

console.log(strInt);
console.log(strReal);
console.log(strArr);

//.toString()
var number = 21;
console.log(number.toString());
var array = [1, 2];
console.log(array.toString());

//Number([String])
var number1 = Number("98");
var number2 = Number("5.7");
var number3 = Number("4 5");

console.log(number1)
console.log(number2)
console.log(number3)

//parseInt([String]) dan parseFloat([String])
var int = '98'
var real = '56.7'
var strInt_1 = parseInt(int);
console.log(strInt_1)
var strInt_2 = parseInt(real)
console.log(strInt_2)
var strReal_1 = parseFloat(int)
console.log(strReal_1)
var strReal_2 = parseFloat(real)
console.log(strReal_2)

//