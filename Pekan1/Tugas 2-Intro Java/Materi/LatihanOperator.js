//Operator Adalah karakter khusu yang merepresentasikan sebuah tindakan. Operator terbagi ke dalam beberap jenis :
// 1.Operator Aritmatika Operator yang melibatkan operasi matematika seperti tambah, kurang, kali, bagi.
// Tambah (+)
// Kurang (–)
// Kali (*)
// Bagi (/)
// Modulus (%)
// Modulus adalah sisa bagi. Contohnya 5%3 hasilnya adalah 2, 100%5 hasilnya 0.
// 2.Operator Assignment (=), Operator untuk mendaftarkan atau meng-assign suatu nilai ke dalam suatu variable
var angka
angka = 11
//3.Operator Perbandingan, Operator yang membandingkan suatu nilai dengan nilai yang lain. Hasil dari perbandingan ini akan dikembalikan dalam tipe data boolean true atau false.
//Equal Operator
var angka1 = 100
console.log(angka1 == 100)
console.log(angka1 == 20)
//Not Equal
var sifat = "Rajin"
console.log(sifat != "males")
console.log(sifat != "bandel")
//Strict Equal ( === ) Selain membandingkan dua nilai nya, strict equal juga membandingkan tipe datanya apakah sama atau tidak
var angka2 = 8
console.log(angka2 == "8")
console.log(angka2 === "8")
console.log(angka2 === 8)
//Strict Not Equal kebalikan dari Strict Equal
var angka3 = 11
console.log(angka3 != "11")
console.log(angka3 !== "11")
console.log(angka3 !== 11)
//Kurang dari & Lebih Dari ( <, >, <=, >=)
var number = 17
console.log(number < 20)
console.log(number > 17)
console.log(number >= 17)
console.log(number <= 20)
//4.Operator Kondisional, Operator yang mengkombinasikan dua nilai kebenaran . Terdapat operator AND (&&) dan OR (||)
//OR(||)
console.log(true || true);
console.log(true || false);
console.log(false || true);
console.log(false || true);
//AND(&&)
console.log(true && true);
console.log(true && false);
console.log(false && false);
console.log(false && true && true);
console.log(true && true && true);