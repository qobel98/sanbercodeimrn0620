//Kondisional dengan if/ else if / else
//Contoh 1 Menjalankan Kode Jika Premis Bernilai true
if (true) {
    console.log("Jalankan code")
}
//Contoh 2 Kode tidak Jalankan Jika Premis Bernilai false
if (false) {
    console.log("code tidak Jalankan")
}
//Contoh 3 Premis dengan perbandingan suatu nilai
var mood = " Happy"
if (mood == "Happy") {
    console.log("Hari Ini Aku Bahagia")
}

//Contoh 4 Branching Sederhana
var minimarketStatus = "open"
if (minimarketStatus == "open") {
    console.log("Saya akan membeli telor dan buah")
} else {
    console.log("minimarket tutup")
}

//Contoh 5 Branching dengan kondisi
var minimarketStatus = "close"
var minuteRemainingToOpen = 5
if (minimarketStatus == "Open") {
    console.log("Saya akan membeli telur dan buah")
} else if (minuteRemainingToOpen <= 5) {
    console.log("Minimarket buka sebentar lagi, saya tungguin")
} else {
    console.log("minimarket tutup, saya pulang lagi")
}

//Contoh 6 Kondisional Bersarang
var minimarketStatus = "Open"
var telur = "SoldOut"
var buah = "Soldout"
if (minimarketStatus == "Open") {
    console.log("saya akan membeli telur dan buah")
    if (telur == "soldout" || buah == "soldout") {
        console.log("Belenjaan Saya tidak lengkap")
    } else if (telur == "Soldout") {
        console.log("telur Habis")
    } else if (buah == "Soldout") {
        console.log("Buah Habis")
    }
} else {
    console.log("minimarket tutup, saya pulang lagi")
}

//Kondisional dengan Switch Case
var buttonPushed = 1
switch (buttonPushed) {
    case 1: {
        console.log('Matikan TV!');
        break;
    }
    case 2: {
        console.log('Turunkan Volume Tv!');
        break;
    }
    case 3: {
        console.log('Tingkatkan Volume Tv!');
        break;
    }
    case 4: {
        console.log('Matikan Suara Tv!');
        break;
    }
    default: {
        console.log('Tidak Terjadi Apaapa');
        break;
    }
}