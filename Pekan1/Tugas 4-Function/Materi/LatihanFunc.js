//Function adalah sebuah blok kode yang disusun sedemikian rupa untuk menjalankan sebuah tindakan.
//blok kode ini dibuat untuk dapat bisa digunakan kembali.Cara penulisanya seperti berikut:


// function Nama_function(Parameter 1, parameter 2, ....){
//     [isi dari function berupa tindakan]
//     return [expression];
// }

//Contoh FUnction 1: Function Sederhana tanpa Return
function tampilkan() {
    console.log("Hallo! Salam Lidunya")
}
tampilkan();

//Contoh Function 2 : Function Sederhana dengan return
function munculkanAngkaDua() {
    return 2
}

var tampung = munculkanAngkaDua();
console.log(tampung)

//contoh Function 3: funnction dengan parameter
function kalikanDua(angka) {
    return angka * 2
}

var tampung2 = kalikanDua(2)
console.log(tampung2)

//Contoh Function 4: Pengiriman parameter lebih dari satu
function tampilkanAngka(angkaPertama, angkaKedua) {
    return angkaPertama * angkaKedua
}
console.log(tampilkanAngka(5, 3))

//Contoh Function 5: Inisialisasi parameter dengan nilai default
function tampilkanAngka5(angka = 11) {
    return angka
}
console.log(tampilkanAngka5(5))
console.log(tampilkanAngka5())

//Kita juga dapat menampung function sebagai variable dengan sebuah bentuk function yang dinamakan Anonymous Function.
var fungsiPerkalian = function (angkaKesatu, angkaKedua) {
    return angkaKesatu * angkaKedua
}
console.log(fungsiPerkalian(2, 4))